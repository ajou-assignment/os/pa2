#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sys/queue.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>

#define FORCE_INLINE __attribute__((always_inline)) inline
#define NUM_CORES 4
int NUM_THREADS = (NUM_CORES * 2 + 1);
const unsigned long long MAX_MEM = 1 * 1024 * 1024 * 1024LL / 4;
#define HASHTABLE_SIZE 0x100000

#define MAX_SIZE_PER_THREAD (MAX_MEM / NUM_THREADS)

#define STR_LEN 256
#define WORD_LEN 256

#pragma GCC diagnostic ignored "-Wformat"
//#pragma GCC diagnostic ignored "-Wmacro-redefined"
#define RELEASE
#ifdef DEBUG
#pragma GCC diagnostic ignored "-Wformat-extra-args"
#define LOG(format, args...) fprintf(stderr, format, args)
#else
#define LOG(format, args...) (void)0
#endif

#define CHECK_ERR(x) \
  if (x == -1)       \
  {                  \
    perror("Error"); \
  }
#define lock(x)               \
  char UNIQUE_VAR;            \
  for (UNIQUE_VAR = 1,        \
      pthread_mutex_lock(&x); \
       UNIQUE_VAR == 1;       \
       --UNIQUE_VAR, pthread_mutex_unlock(&x))

SLIST_HEAD(listhead, entry);
struct entry
{
  char name[WORD_LEN];
  int frequency;
  SLIST_ENTRY(entry)
  entries;
};

struct hashtable_t
{
  int hash_size;
  int width;
  int num_entries;
  struct listhead **table;
} g_hashtable;

typedef unsigned int hash_t;

struct range_t
{
  unsigned long long start;
  unsigned long long size;
} ranges[256];

const char *WHITE_SPACE = " \t\n";
char *input_filename;

struct hashtable_t *hashtables;
int num_files = 0;

#define BIG_CONSTANT(x) (x##LLU)

#define getblock(p, i) (p[i])

FORCE_INLINE unsigned int hash32(char *str)
{

  unsigned int hash = 0x12345678;
  unsigned int x = 0x8654321;
  unsigned char c;
  while ((c = *str++))
  {
    hash = (hash << 4) + c;

    if ((x = hash & 0xF0000000L) != 0)
    {
      hash ^= (x >> 24);
    }

    hash &= ~x;
  }
  hash = hash % HASHTABLE_SIZE;
  return hash;
}

FORCE_INLINE struct entry *create_word(char *name, int frequency)
{
  struct entry *e = malloc(sizeof(*e));
  memset(e, 0, sizeof(*e));
  strcpy(e->name, name);
  e->frequency = frequency;
  return e;
}

void clean_words(struct hashtable_t *hashtable, int entry_no_free)
{
  int i;
  for (i = 0; i < hashtable->hash_size; i++)
  {
    int idx = 0;
    struct listhead *words = hashtable->table[i];

    if (!entry_no_free)
    {
      while (!SLIST_EMPTY(words))
      {
        struct entry *n1 = SLIST_FIRST(words);
        SLIST_REMOVE_HEAD(words, entries);
        free(n1);
      }
    }
    free(words);
  }
  free(hashtable->table);
  // clean up
}

void init_hashtable(struct hashtable_t *hashtable)
{
  int i;
  hashtable->hash_size = HASHTABLE_SIZE;
  hashtable->table = malloc(sizeof(*hashtable->table) * hashtable->hash_size);
  memset(hashtable->table, 0, sizeof(*hashtable->table) * hashtable->hash_size);
  hashtable->num_entries = 0;
  struct listhead **table = hashtable->table;
  for (i = 0; i < hashtable->hash_size; i++)
  {
    table[i] = malloc(sizeof(*table));
    SLIST_INIT(table[i]);
  }
}

FORCE_INLINE struct entry *get_word(struct hashtable_t *hashtable, char *name)
{
  hash_t hash = hash32(name);

  struct entry *word;
  SLIST_FOREACH(word, hashtable->table[hash], entries)
  {
    if (!word)
      return NULL;
    if (!strcmp(word->name, name))
      return word;
  }
  return NULL;
}

FORCE_INLINE struct entry *set_word(struct hashtable_t *hashtable, char *name, int frequency, struct entry *w)
{
  hash_t hash = hash32(name);

  struct listhead *head = hashtable->table[hash];
  struct entry *word = SLIST_FIRST(head);

  if (!word)
  {
    if (!w)
      w = create_word(name, frequency);
    SLIST_INSERT_HEAD(head, w, entries);
    hashtable->num_entries++;
    return w;
  }
  int idx = 0;

  for (; word; word = SLIST_NEXT(word, entries))
  {
    if (!strcmp(word->name, name))
    {
      strcpy(word->name, name);
      word->frequency = frequency;
      return word;
    }
    if (!w)
      w = create_word(name, frequency);
    SLIST_INSERT_AFTER(word, w, entries);
    hashtable->num_entries++;
    return word;
  }
  LOG("COLLISION ERROR\n", "");
  exit(-1);
  return NULL;
}

struct entry **table_to_arr(struct hashtable_t *hashtable, struct entry **arr)
{

  int i;
  for (i = 0; i < hashtable->hash_size; i++)
  {
    struct entry *word;
    SLIST_FOREACH(word, hashtable->table[i], entries)
    {
      *arr++ = word;
    }
  }
  return arr;
}

FORCE_INLINE void lower(char *buf)
{
  int line_length = strlen(buf);

  for (int it = 0; it < line_length; ++it)
  {
    if (!isalnum(buf[it]))
    {
      buf[it] = ' ';
    }
    else
    {
      buf[it] = tolower(buf[it]);
    }
  }
}

FORCE_INLINE unsigned long long get_file_size(int fd)
{
  unsigned long long file_size = lseek(fd, 0, SEEK_END);
  lseek(fd, 0, SEEK_SET);
  return file_size;
}

int split_files(char *filename)
{
  int fd = open(filename, O_RDONLY);
  if (fd == -1)
    return -1;

  unsigned long long file_size = get_file_size(fd);
  if (file_size < 100 * 1024 * 1024)
  {
    NUM_THREADS = 1;
  }
  unsigned long long start = 0;
  unsigned long long offset = start;
  int file_idx = 0;
  unsigned long long size_per_thread = file_size / NUM_THREADS;
  if (size_per_thread > MAX_SIZE_PER_THREAD)
    size_per_thread = MAX_SIZE_PER_THREAD;
  while (offset < file_size)
  {
    char buf[WORD_LEN + 1] = {0};
    char *last;
    char *token;
    start = offset;
    offset += size_per_thread;
    lseek(fd, offset, SEEK_SET);
    CHECK_ERR(read(fd, buf, WORD_LEN));
    token = strtok_r(buf, WHITE_SPACE, &last);
    token = strtok_r(NULL, WHITE_SPACE, &last);
    if (offset < file_size)
      offset += token - (char *)buf;
    else
      offset = file_size;
    unsigned long long size_per_file = offset - start;

    LOG("[file %d] %llx +%llx \n", file_idx, start, size_per_file);
    ranges[file_idx].start = start;
    ranges[file_idx].size = size_per_file;
    file_idx++;
  }
  close(fd);
  return file_idx;
}

struct hashtable_t *map_thread(long tidx)
{
  int file_idx = tidx;
  int fd = open(input_filename, O_RDONLY);
  if (fd == -1)
  {
    perror(input_filename);
    exit(-1);
  }
  unsigned long long file_size = ranges[file_idx].size;
  char *buf = malloc(file_size + 1);
  struct hashtable_t *hashtable = &hashtables[tidx];
  init_hashtable(hashtable);
  for (file_idx = tidx; file_idx < num_files; file_idx += NUM_THREADS)
  {
    if (file_size < ranges[file_idx].size)
    { //expand
      buf = realloc(buf, file_size + 1);
    }
    file_size = ranges[file_idx].size;
    memset(buf, 0, file_size + 1);

    LOG("[File %d] Start\n", file_idx);
    unsigned long long current = lseek(fd, ranges[file_idx].start, SEEK_SET);
    CHECK_ERR(read(fd, buf, file_size));
    LOG("[File %d] Reading Finished (%llx ~ %lx)\n", file_idx, current, lseek(fd, 0, SEEK_CUR));
    lower(buf);
    LOG("[File %d] Start Counting\n", file_idx);

    char *last = NULL;

    // Tokenization
    char *tok = strtok_r(buf, WHITE_SPACE, &last);

    int num_entries = 0;

    do
    {

      if (tok == NULL || strcmp(tok, "") == 0)
      {
        continue;
      }
      struct entry *w;

      w = get_word(hashtable, tok);
      if (w)
        w->frequency++;
      else
      {

        w = set_word(hashtable, tok, 1, NULL);
        num_entries++;
      }

    } while ((tok = strtok_r(NULL, WHITE_SPACE, &last)));

    LOG("[File %d] Finished\n", file_idx);
  }
  free(buf);
  close(fd);
  return hashtable;
}

int compare(const void *aa, const void *bb)
{
  struct entry *a = *(struct entry **)aa;
  struct entry *b = *(struct entry **)bb;
  if (a->frequency < b->frequency)
    return 1;
  else if (a->frequency > b->frequency)
    return -1;
  return strcmp(a->name, b->name);
}

void reduce(struct hashtable_t *hashtables)
{
  long i, j;
  struct hashtable_t result_hashtable;
  init_hashtable(&result_hashtable);
  LOG("** Merging Start\n", "");
  int num_entries = 0;
  for (j = 0; j < NUM_THREADS; j++)
  {
    struct hashtable_t *hashtable = &hashtables[j];
    for (i = 0; i < hashtable->hash_size; i++)
    {
      int idx = 0;
      struct entry *word;
      SLIST_FOREACH(word, hashtable->table[i], entries)
      {
        if (!word)
          break;
        struct entry *w = get_word(&result_hashtable, word->name);

        if (w)
        {
          w->frequency += word->frequency;
          //leak TODO : fix
        }
        else
        {

          w = set_word(&result_hashtable, word->name, word->frequency, NULL);
          num_entries++;
        }
      }
    }
    clean_words(hashtable, 0);
  }
  LOG("** Merging Finished\n", "");

  struct entry **entry_arr = malloc(sizeof(*entry_arr) * num_entries);
  LOG("Table To Array\n", "");
  table_to_arr(&result_hashtable, entry_arr);
  LOG("** Sorting Start\n", "");
  qsort(entry_arr, num_entries, sizeof(*entry_arr), compare);
  LOG("** Sorting Finished\n", "");
  for (i = 0; i < num_entries; i++)
  {
    printf("%s %d\n", entry_arr[i]->name, entry_arr[i]->frequency);
  }
  free(entry_arr);
  clean_words(&result_hashtable, 0);
}

void map(struct hashtable_t *hashtables)
{
  num_files = split_files(input_filename);
  long i;
  pthread_t tids[NUM_THREADS];
  for (i = 0; i < NUM_THREADS; i++)
  {
    pthread_create(&tids[i], NULL, (void *(*)(void *))map_thread, (void *)i);
  }
  LOG("Created [%d] Threads \n", NUM_THREADS);

  for (i = 0; i < NUM_THREADS; i++)
  {
    pthread_join(tids[i], NULL);
  }
}

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    LOG("%s: not enough input\n", argv[0]);
    exit(1);
  }
  num_files = split_files("data/enwiki-latest-all-titles");

  if (argc == 2)
    input_filename = argv[1];
  else
    input_filename = getenv("INPUT");

  hashtables = malloc(sizeof(*hashtables) * NUM_THREADS);
  LOG("** Map Start\n", "");
  map(hashtables);
  LOG("** Reduce Start\n", "");
  reduce(hashtables);
  free(hashtables);
}
