import os
import sys

if len(sys.argv) < 2:
    print 'input path'
    exit(-1)
    
f = open(sys.argv[1])

size = os.fstat(f.fileno()).st_size

result = {}
for line in f:
    s = ''
    for c in line:
        if c.isalnum():
            s += c.lower()
        else:
            s += ' '
    words = s.split()
    for word in words:
        if not word in result:
            result[word] = 0
        result[word] += 1
f.close()

def compare(a, b):
    a_name, a_frequency = a
    b_name, b_frequency = b
    if a_frequency < b_frequency:
        return 1
    elif a_frequency > b_frequency:
        return -1
    if a_name < b_name:
        return -1
    elif a_name > b_name:
        return 1
    return 0

entries = result.items()
entries.sort(cmp=compare)
#print entries
print '\n'.join(map(lambda entry: '{} {}'.format(entry[0], entry[1]), entries))
#print len(entries)
