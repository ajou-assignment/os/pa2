<p style="text-align:center; font-size:2em; font-weight:bold; line-height:1.5em">
    OS Assignment #2
<p>


<p style="text-align:right; font-size:1.2em">
사이버보안학과
201720692 이시훈
</p>


### 이 과제를 어디까지 진행했는지

`owl.txt` `therepublic.txt` `enwiki-latest-all-titles` `fake-data.txt` 모두 카운팅에 성공하였다.

#### 병렬화

MapReduce를 구현하기위해 파일을 여러개로 쪼개서 생성하고 Map에서 읽고 카운팅하여 결과들을 저장하였다. 그 후에 Reduce 쓰레드에서 다시 읽어 합친후 정렬하고 출력하도록 구현하였다.

하지만 파일 생성 시간이 오래걸려 부분적으로 바꾸었다. 메모리가 충분하기 때문에 파일은 쪼개지만 file offset 범위(start, size)들을 구하여 리스트를 생성하고 쓰레드들이 이 리스트들을 읽어 카운팅하게 하였다. 이전 버전과는 달리 카운팅후 결과 파일을 생성하지 않고 메모리에 저장한다. 그후 Reduce를 한 쓰레드에서 처리하게 하였다. 결과파일이 없어 결과파일을 읽지않고 전역변수 리스트를 토대로 reduce하고 table을 linear한 array로 바꾼후 정렬하고 출력하게 하였다.

##### 과정

* 쓰레드들이 한번 처리할때 읽을 블록 범위들을 구함 (읽을 파일 범위를 한꺼번에 구하여 리스트에 저장)
* map thread에서 쓰레드들이 범위 리스트들을 순차적으로 받아 지정된 범위의 파일을 읽음
* 읽은 블록의 내용을 lower, tokenize 하고 hashtable에 단어 빈도수 추가
* 모든 map thread가 끝나면 reduce
* 여러 hashtable을 모두 읽어 한 hashtable에 합침.
* hashtable을 array로 변환.
* quick sort
* 출력



### 자료구조

Hash Table과 Single Linked list를 사용하였다. Hash 알고리즘을 선정하는데 instruction 대비 충돌 비율이 작은 함수로 선택하였고 collision이 발생하면 linked list에 추가하도록 하였다. linked list가 아닌 fixed linear array를 사용하였지만  wiki에서 메모리가 부족해 linked list로 변경하였다.



#### 정렬

reduce 싱글 쓰레드에서 2차원의 hash table을 순차적으로 순회하여  linear array에 추가하는 방식으로 2차원 hashtable을 1차원 linear array로 변환하였다. 

그후 qsort를 사용하여 한꺼번에 정렬하였다. 

reduce에서 여러 쓰레드를 한 테이블로 합치는데에서 시간이 가장 오래걸린다.



### 이 과제를 수행하면서 배운 것

#### Hash Table

자료구조및실습 과목을 수강하고 있어 아직 배우지 않았는데 해시테이블을 처음 구현해서 사용해보았다.

hash collision이 발생할때 알고리즘이 다양하다. 다음 비는 곳을 찾아 넣거나. array를 쓰거나 liked list를 쓴다. list를 사용하였다.



#### **Map Reduce**

병렬화 알고리즘으로 파일을 쪼개서 쓰레딩으로 처리한후 다시 합치는 병렬처리이다.



#### ETC

* `strtok`은 thread-unsafe 함수이므로 쓰레딩 시에는 `strtok_r`을 사용해야한다.
* `strtok_r`은 implicit declared가 발생할수 있으므로 `-std=gnu99`로 컴파일해야한다. 이번 과제에서는 컴파일 옵션을 바꿀수 없기 때문에 `#define _GNU_SOURCE ` 으로 선언하여 대체하였다.
* 1GB파일을 위해 기타 메모리 관리를 더 신경쓰게 되었다.



### 이 과제에 대한 피드백

* 1GB 파일에 대한 설명이 부족했던것 같습니다.
  * 기존 코드를 유지하려고 해보았으나 결국에는 자료구조를 바꿀수 밖에 없었습니다. ㅠㅠ
  * 기존 코드를 유지한후 처리하는 방법이 있겠지만 그대로 사용해도 된다고 하셔서 고민을 많이했네요
* 항상 수고 많으십니다.
  * 감사합니다.
